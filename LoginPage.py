from selenium.webdriver.common.by import By

from BasePage import BasePage
from InventoryPage import InventoryPage


class LoginPage(BasePage):
    URL = "https://www.saucedemo.com/"
    USER_NAME_INPUT = (By.ID, 'user-name')
    PASSWORD_INPUT = (By.ID, 'password')
    LOGIN_BUTTON = (By.ID, 'login-button')

    def type_user_name(self, user_name="standard_user"):
        self.find_element(self.USER_NAME_INPUT).send_keys(user_name)

    def type_password(self, password="secret_sauce"):
        self.find_element(self.PASSWORD_INPUT).send_keys(password)

    def click_login_button(self):
        self.find_element(self.LOGIN_BUTTON).click()
        return InventoryPage(self.driver)
