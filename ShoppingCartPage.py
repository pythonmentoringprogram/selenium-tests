from selenium.webdriver.common.by import By

from BasePage import BasePage


class ShoppingCartPage(BasePage):

    CART_ITEM_XPATH = "//div[@id='cart_contents_container']//div[@class='inventory_item_name' and text()='{}']"
    REMOVE_ITEM_BUTTON_XPATH = "//div[@class='cart_item' and .//div[@class='inventory_item_name' and text()='{}']]//button[text()='Remove']"

    def remove_item_from_cart(self, item_name):
            self.find_element((By.XPATH, self.REMOVE_ITEM_BUTTON_XPATH.format(item_name))).click()

    def is_item_present_in_cart(self, item_name):
        return len(self.find_elements((By.XPATH, self.CART_ITEM_XPATH.format(item_name)))) > 0
