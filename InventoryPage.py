from selenium.webdriver.common.by import By

from BasePage import BasePage
from MenuPanel import MenuPanel
from ShoppingCartPage import ShoppingCartPage


class InventoryPage(BasePage):
    URL = "https://www.saucedemo.com/inventory.html"

    INVENTORY_ITEMS = (By.CLASS_NAME, "inventory_item")
    INVENTORY_ITEM_XPATH = "//div[@class='inventory_item' and .//div[text()='{}']]"
    ADD_TO_CART_BUTTON_XPATH = ".//button[text()='Add to cart']"
    SHOPPING_CART_BUTTON = (By.ID, 'shopping_cart_container')
    INVENTORY_ITEM_PRICES = (By.CLASS_NAME, "inventory_item_price")
    INVENTORY_ITEM_NAMES = (By.CLASS_NAME, "inventory_item_name")
    SORT_CONTAINER = (By.CLASS_NAME, "product_sort_container")
    SORT_CONTAINER_OPTION_XPATH = "//select[@class='product_sort_container']/option[@value='{}']"

    def __init__(self, driver):
        super().__init__(driver)
        self.MENU_PANEL = MenuPanel(driver)

    def add_item_to_cart(self, item_name):
        self.find_element((By.XPATH, self.INVENTORY_ITEM_XPATH.format(item_name))).find_element(By.XPATH, self.ADD_TO_CART_BUTTON_XPATH).click()

    def open_shopping_cart(self):
        self.find_element(self.SHOPPING_CART_BUTTON).click()
        return ShoppingCartPage(self.driver)

    def sort_by(self, sort_value):
        self.find_element(self.SORT_CONTAINER).click()
        self.find_element((By.XPATH, self.SORT_CONTAINER_OPTION_XPATH.format(sort_value))).click()

    def get_item_names(self):
        return list(map(lambda x: x.text, self.find_elements(self.INVENTORY_ITEM_NAMES)))

    def get_item_prices(self):
        return list(map(lambda x: x.text.replace('$', ''), self.find_elements(self.INVENTORY_ITEM_PRICES)))
