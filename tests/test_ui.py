from selenium import webdriver

from InventoryPage import InventoryPage
from LoginPage import LoginPage

SAUCE_LABS_ONESIE = "Sauce Labs Onesie"
SAUCE_LABS_FLEECE_JACKET = "Sauce Labs Fleece Jacket"


def test_login_process(browser):
    login_page = LoginPage(browser)
    login_page.type_user_name()
    login_page.type_password()
    inventory_page = login_page.click_login_button()

    assert browser.current_url == inventory_page.URL


def test_logout_process(browser):
    login_page = LoginPage(browser)
    login_page.type_user_name()
    login_page.type_password()
    inventory_page = login_page.click_login_button()

    inventory_page.MENU_PANEL.logout()

    assert browser.current_url == login_page.URL


def test_sorting_items(browser):
    login_page = LoginPage(browser)
    login_page.type_user_name()
    login_page.type_password()
    inventory_page = login_page.click_login_button()

    inventory_page.sort_by('az')
    names = inventory_page.get_item_names()
    assert names == sorted(names)
    inventory_page.sort_by('za')
    names = inventory_page.get_item_names()
    assert names == sorted(names, reverse=True)
    inventory_page.sort_by('lohi')
    prices = inventory_page.get_item_prices()
    assert prices == sorted(prices, key=lambda x: float(x))
    inventory_page.sort_by('hilo')
    prices = inventory_page.get_item_prices()
    assert prices == sorted(prices, key=lambda x: float(x), reverse=True)

def test_adding_items_to_cart(browser):
    login_page = LoginPage(browser)
    login_page.type_user_name()
    login_page.type_password()
    inventory_page = login_page.click_login_button()

    inventory_page.add_item_to_cart(SAUCE_LABS_FLEECE_JACKET)
    inventory_page.add_item_to_cart(SAUCE_LABS_ONESIE)

    shopping_cart = inventory_page.open_shopping_cart()
    assert shopping_cart.is_item_present_in_cart(SAUCE_LABS_FLEECE_JACKET)
    assert shopping_cart.is_item_present_in_cart(SAUCE_LABS_ONESIE)


def test_remove_items_from_cart(browser):
    login_page = LoginPage(browser)
    login_page.type_user_name()
    login_page.type_password()
    inventory_page = login_page.click_login_button()

    inventory_page.add_item_to_cart(SAUCE_LABS_FLEECE_JACKET)
    inventory_page.add_item_to_cart(SAUCE_LABS_ONESIE)

    shopping_cart = inventory_page.open_shopping_cart()
    assert shopping_cart.is_item_present_in_cart(SAUCE_LABS_FLEECE_JACKET)
    assert shopping_cart.is_item_present_in_cart(SAUCE_LABS_ONESIE)

    shopping_cart.remove_item_from_cart(SAUCE_LABS_FLEECE_JACKET)
    assert not shopping_cart.is_item_present_in_cart(SAUCE_LABS_FLEECE_JACKET)

