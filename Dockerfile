FROM joyzoursky/python-chromedriver:3.9-selenium

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY . /code/selenium_tests

CMD ["pytest"]