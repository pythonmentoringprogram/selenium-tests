from selenium.webdriver.common.by import By

from BasePage import BasePage


class MenuPanel(BasePage):
    LOGOUT_BUTTON = (By.ID, "logout_sidebar_link")
    MENU_BUTTON = (By.ID, "react-burger-menu-btn")

    def logout(self):
        self.find_element(self.MENU_BUTTON).click()
        self.find_element(self.LOGOUT_BUTTON).click()
